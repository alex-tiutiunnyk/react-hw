import React from "react";
import "./App.css";
import Chat from "./components/Chat";
import logo from "./logo.svg";

function App() {
  const string = "https://edikdolynskyi.github.io/react_sources/messages.json";

  return (
    <div>
      <div className="logo">
        <img src={logo} alt="logo" />
        <h3>Messenger</h3>
      </div>
      <Chat url={string} />
    </div>
  );
}

export default App;
