import React, { useContext } from "react";
import "./OwnMessage.css";
import Context from "../context";
import "../../node_modules/moment/min/moment-with-locales.min.js";
import moment from "moment";

function OwnMessage({ message }) {
  const { removeMessage, editMessage } = useContext(Context);

  return (
    <div className="own-message">
      <div>
        <p className="message-text">{message.text}</p>
      </div>
      <div className="right-block">
        <p className="message-time">{moment(message.createdAt).format("LT")}</p>
        <button
          className="message-edit"
          onClick={editMessage.bind(null, message)}
        >
          Edit &#9998;
        </button>
        <button
          className="message-delete"
          onClick={removeMessage.bind(null, message.id)}
        >
          Delete &#9746;
        </button>
      </div>
    </div>
  );
}

export default OwnMessage;
