import React from "react";
import "./Message.css";
import moment from "moment";

function Message({ message, onClick }) {
  const classes = [];

  var className = message.isLiked ? "message-like liked" : "message-like";
  if (message.isLiked) {
    classes.push("liked");
  }
  return (
    <div className="message">
      <div className="left-block">
        <p className="message-user-name">{message.user}</p>
        <img className="message-user-avatar" src={message.avatar} />
      </div>
      <div>
        <p className="message-text">{message.text}</p>
      </div>
      <div className="right-block">
        <p className="message-time">
          {message.editedAt === ""
            ? moment(message.createdAt).format("LLL")
            : moment(message.editedAt).format("LLL")}
        </p>
        <button className={className} onClick={() => onClick(message.id)}>
          Like &hearts;
        </button>
      </div>
    </div>
  );
}

export default Message;
