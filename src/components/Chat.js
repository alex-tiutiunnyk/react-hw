import React, { useEffect } from "react";
import Header from "./Header";
import MessageInput from "./MessageInput";
import MessageList from "./MessageList";
import Context from "../context";
import Preloader from "./Preloader";

function Chat(props) {
  const [messages, setMessages] = React.useState([]);
  const [loading, setLoading] = React.useState(true);
  const [currentMessage, setCurrentMessage] = React.useState(null);

  useEffect(() => {
    fetch(props.url)
      .then((response) => response.json())
      .then((messages) => {
        setTimeout(() => {
          setMessages(messages);
          setLoading(false);
        }, 2000);
      });
  }, []);

  function likeMessage(id) {
    setMessages(
      messages.map((message) => {
        if (message.id === id) {
          message.isLiked = !message.isLiked;
        }
        return message;
      })
    );
  }

  function addMessage(text) {
    var date = new Date();
    setMessages(
      messages.concat([
        {
          text,
          id: Date.now(),
          userId: 18,
          createdAt: date,
          editedAt: "",
        },
      ])
    );
  }

  function editMessage(message) {
    setCurrentMessage({ ...message });
  }

  function editSubmitMessage(message) {
    let currentIndex = messages.findIndex((x) => x.id == message.id);
    if (currentIndex > -1) messages[currentIndex].text = message.text;
    setCurrentMessage(null);
    setMessages(messages);
  }

  function removeMessage(id) {
    setMessages(messages.filter((message) => message.id !== id));
  }

  return (
    <Context.Provider value={{ removeMessage, editMessage }}>
      <div>
        {loading ? null : <Header messages={messages} />}
        <MessageList messages={messages} onLike={likeMessage} />
        {loading && <Preloader />}
        {messages.length ? (
          <MessageInput
            onCreate={addMessage}
            key={currentMessage?.id ?? -1}
            currentMessage={currentMessage}
            onEdit={editSubmitMessage}
          />
        ) : loading ? null : (
          <p style={{ textAlign: "center" }}>Chat is empty!</p>
        )}
      </div>
    </Context.Provider>
  );
}

export default Chat;
