import React, { useState, useEffect } from "react";
import "./MessageInput.css";

function MessageInput({ onCreate, onEdit, currentMessage }) {
  const [value, setValue] = useState("");

  useEffect(() => {
    if (currentMessage != null && value == "") setValue(currentMessage.text);
  });

  function submitHandler(event) {
    event.preventDefault();
    if (value.trim()) {
      onCreate(value);
      setValue("");
    }
  }

  function submitEditHandler(event) {
    event.preventDefault();
    if (value.trim()) {
      onEdit({ ...currentMessage, text: value });
      setValue("");
    }
  }

  return (
    <form
      className="message-input"
      onSubmit={currentMessage != null ? submitEditHandler : submitHandler}
    >
      <input
        className="message-input-text"
        type="text"
        placeholder="Message"
        value={value}
        onChange={(event) => {
          event.preventDefault();
          setValue(event.target.value);
        }}
      />
      <button className="message-input-button" type="submit">
        Send
      </button>
    </form>
  );
}

export default MessageInput;
