import React from "react";
import { Divider } from "rsuite";
import "rsuite/dist/styles/rsuite-default.css";
import "./MessageList.css";
import Message from "./Message";
import OwnMessage from "./OwnMessage";
import moment from "moment";

function MessageList(props) {
  let previousDate = null;
  return (
    <div className="message-list">
      {props.messages.map((message) => {
        let divider = false;

        let currentDate = new Date(Date.parse(message.createdAt));
        if (
          previousDate == null ||
          previousDate.getFullYear() !== currentDate.getFullYear() ||
          previousDate.getMonth() !== currentDate.getMonth() ||
          previousDate.getDay() !== currentDate.getDay()
        ) {
          previousDate = currentDate;
          divider = true;
        }

        if (message.userId !== 18)
          return (
            <div>
              {divider ? (
                <Divider>
                  {moment(message.createdAt).format("MMMM Do YYYY")}
                </Divider>
              ) : null}
              <Message
                message={message}
                key={message.id}
                onClick={props.onLike}
              />
            </div>
          );
        else
          return (
            <div>
              {divider ? (
                <Divider>
                  {moment(message.createdAt).format("MMMM Do YYYY")}
                </Divider>
              ) : null}
              <OwnMessage message={message} key={message.id} />
            </div>
          );
      })}
    </div>
  );
}

export default MessageList;

//    { props.messages.map(message => {
//        return <Message
//         message={message}
//         key={message.id}
//         onClick={props.onLike} />
//   })}

/* <Divider>Today</Divider> */
