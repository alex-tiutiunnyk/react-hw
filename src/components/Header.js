import React from "react";
import "./Header.css";
import "../../node_modules/moment/min/moment-with-locales.min.js";
import moment from "moment";

function Header({ messages }) {
  var size = messages.length;
  const userCount = [];
  const messageTime = [];

  messages.map((message) => {
    if (userCount.indexOf(message.userId) === -1) {
      userCount.push(message.userId);
    }
  });

  messages.map((message) => {
    if (message.editedAt == "") {
      messageTime.push(message.createdAt);
    } else {
      messageTime.push(message.editedAt);
    }
  });
  var length = messageTime.length;

  return (
    <div className="header">
      <div>
        <p className="header-title">My chat</p>
        <p className="header-users-count">{userCount.length} participants</p>
        <p className="header-messages-count">{size} messages</p>
      </div>
      <div>
        <p className="header-last-message-date">
          last message at {moment(messageTime[length - 1]).format("LLL")}
        </p>
      </div>
    </div>
  );
}

export default Header;
